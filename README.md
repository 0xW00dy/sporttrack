# SportTrack

### Qu'est ce que SportTrack ?
C'est une application Web permettant à des sportifs de conserver et de gérer des données de position et de fréquence cardiaque issues de leur montre “cardio/gps”.
#
### [- architecture de l'application -]
```
sporttrack/
├── Dockerfile
├── js
│   ├── sport-track-db
│   │   ├── package.json
│   │   ├── package-lock.json
│   │   └── SportTrack.db
│   └── src
│       └── js
│           └── fonctions.js
├── README.md
└── src
    ├── contents
    │   ├── icone.png
    │   ├── logo.png
    │   └── photo_background.jpg
    ├── controllers
    │   ├── AddUserController.php
    │   ├── ApplicationController.php
    │   ├── ConnectUserController.php
    │   ├── Controller.php
    │   ├── DisconnectUserController.php
    │   ├── ListActivityController.php
    │   ├── MainController.php
    │   ├── ModifyUserController.php
    │   └── UploadActivityController.php
    ├── create_db.sql
    ├── css
    │   └── stylesheet.css
    ├── index.php
    ├── model
    │   ├── ActivityDAO.php
    │   ├── Activity.php
    │   ├── CalculDistanceImpl.php
    │   ├── CalculDistance.php
    │   ├── DataDAO.php
    │   ├── Data.php
    │   ├── JSONUtility.php
    │   ├── SqliteConnection.php
    │   ├── UserDAO.php
    │   └── User.php
    ├── SportTrack.db
    ├── test
    │   └── sqlite_connection_test.php
    └── views
        ├── AddUserForm.php
        ├── ConnectUserForm.php
        ├── ConnectUserValidationView.php
        ├── CreateAccountForm.php.bak
        ├── DataForm.php
        ├── DisconnectUserVue.php
        ├── ErrorView.php
        ├── ListActivityView.php
        ├── MainView.php
        ├── ModifyUserView.php
        └── UploadActivityForm.php

11 directories, 44 files
```

#

### [- Architecture de la base de données -]

#### Table Users
```
| id | firstname | lastname | birthday | gender | height | weight | email | password |
```

#### Table Activity
```
| idActivity | dateActivity | description | idUser |
```

#### Table Data
```
| idData | duration | cardioFreqAvg | distance | idActivity |
```
#

### [- Comment créer le fichier db à partir du SQL -]
Faire cette commande dans un terminal au niveau de l'emplacement du fichier de création de table SQL :
```sh
sqlite3 SportTrack.db < create_db.sql
```

### [- Lancement du Docker -]
Il faut réaliser toutes les commandes à la racine du dossier (donc dans le dossier sporttrack)
Pour construire le Docker :
```sh
sudo docker build . -t sporttrack
```
Pour lancer le Docker :
```sh
sudo docker run -v {le path jusqu au dossier sporttrack}/src:/var/www/html -p 80:80 sporttrack
```
Si il y a des problèmes de droit vous pouvez utiliser :
```sh
sudo docker ps
    ...
sudo docker exec -it {2 premiers nombres/lettre de l id du docker} /bin/bash
```

### [- Acceder au site -]
Ouvrer votre navigateur et allez à l'URL suivant : [http://localhost/](http://localhost/)



## Fonctionnement de l'application
#
### [- Accueil -]
Le bouton home permet de retourner sur l'accueil de l'application.

### [- Créer un compte -]
Le bouton register permet de s'inscrire sur le site et enregistrer les informations de l'utilisateur. Pour ce faire, il faut remplir tous les champs du formulaire et appuyer sur Enregistrer.

### [- Se connecter -]
Le bouton login permet de se connecter avec les informations rentrées lors de l'inscription.

### [- Ajouter des informations d'activité -]
Pour ajouter des informations d'activité sportive, il suffit d'utiliser la page Upload a file. Sur cette page il suffit d'ajouter le fichier en le sélectionnant et cliquer sur Envoyer.

### [- Regarder les informations d'activité ajouté au compte -]
Pour ce faire, il faut cliquer sur le bouton de List of Activities. Les informations ajouté avec les fichiers JSON s'afficheront ici.

### [- Changer mes informations personnelles -]
Pour ce faire, il faut cliquer sur le bouton Change your data et modifier les données des champs que l'on souhaite modifier.
