FROM php:7.2-apache

RUN mkdir -p /var/www/html
COPY src/ /var/www/html
RUN chown -R :www-data /var/www/html
RUN chmod -R 666 /var/www/html
#COPY src/ /usr/local/apache2/htdocs

EXPOSE 80:80
