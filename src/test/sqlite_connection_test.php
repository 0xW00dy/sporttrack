<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('../model/User.php');
    require_once('../model/UserDAO.php');
    UserDAO::getInstance()->deleteAll();
    $usr = new User();
    $usr->init("Julien", "Blo", "03/01/2020", "M", 180, 75, "julien.blo@gmail.com", "mdp");
    UserDAO::getInstance()->insert($usr);
    echo "findAll:";
    print_r(UserDAO::getInstance()->findAll());
    echo "</br></br>findUser:";
    print_r(UserDAO::getInstance()->findUser("Julien","Blo"));
    echo "</br></br>getEmail:";
    print_r(UserDAO::getInstance()->getEmail());
    echo "</br></br>getUser:";
    print_r(UserDAO::getInstance()->getUser("julien.blo@gmail.com"));
    echo "</br></br>update:";
    $usr->setFirstName("BOB");
    print_r(UserDAO::getInstance()->update($usr));
    echo "</br></br>insert usr2:";
    $usr2 = new User();
    $usr2->init("Patrick", "Balkany", "03/01/1008", "M", 170, 75, "patrick.balkany@vol.argent", "superMDP");
    UserDAO::getInstance()->insert($usr2);
    print_r(UserDAO::getInstance()->findAll());
    UserDAO::getInstance()->delete($usr);
    echo "</br></br>delete usr1:";
    print_r(UserDAO::getInstance()->findAll());
?>
