<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/css/stylesheet.css">
        <link href="/contents/icone.png" type="image/png" rel="icon">
        <title>
            SportTrack
        </title>

    </head>

    <body>
        <div class="head">
            <img src="/contents/logo.png" width="125" height="125" rel="logo" type="image/png">
            <h1>SportTrack</h1>
        </div>
        <?php
		    if(isset($_SESSION["user"])) {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		        echo '</br><a href="?page=upload_activity_form">Upload a file</a>';
                echo '</br><a href="?page=list_activities">List of Activities</a>';
		        echo '</br><a href="?page=modify_user_form">Change your data</a>';
                echo '</br><a href="?page=user_disconnect">Disconnect</a></div>';
		    } else {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		    	echo '</br><a href="?page=user_add_form">Register</a>';
		    	echo '</br><a href="?page=user_connect">Login</a></div>';
		    }
    	?>
        <div class="content">  
            <h1>SportTrack</h1>
            <h2>Vos données</h2>
            <div class="data_form">
                <form method="post">
                    Nom<input type="text" name="lastname" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getLastName()); ?> /><br />
                    Prénom<input type="text" name="firstname" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getFirstName()); ?> /><br />
                    Date de naissance<input type="date" name="birthday" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getBday()); ?> /><br />
                    Sexe<select name="gender" value=<?php echo($_SESSION["user"][0]->getGender()); ?> >
                        <option>M</option>
                        <option>F</option>
                        <option>O</option>
                    </select><br />
                    Taille<input type="number" name="height" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getHeight()); ?> /><br />
                    Poids<input type="number" name="weight" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getWeight()); ?> /><br />
                    Email<input type="email" name="email" placeholder=<?php echo htmlspecialchars($_SESSION["user"][0]->getEmail()); ?> /><br />
                    Nouveau mot de passe <input type="newpassword" name="password"/> <br />
                    Mot de passe<input required="true" type="password" name="password"/><br />
                    <input type="submit" value="Enregistrer"/>
                </form>
            </div>
        </div>

        <div class="footer">
            <p>site created by 2 IUT students</p>
        </div>
    </body>
</html>

