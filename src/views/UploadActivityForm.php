<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/../css/stylesheet.css">
        <link href="/../contents/icone.png" type="image/png" rel="icon">
        <title> 
            SportTrack
        </title>
    </head>

    <body>

        <div class="head">
            <img src="/contents/logo.png" width="125" height="125" rel="logo" type="image/png">
            <h1>SportTrack</h1>
        </div>
        <?php
		    if(isset($_SESSION["user"])) {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		        echo '</br><a href="?page=upload_activity_form">Upload a file</a>';
                echo '</br><a href="?page=list_activities">List of Activities</a>';
		        echo '</br><a href="?page=modify_user_form">Change your data</a>';
                echo '</br><a href="?page=user_disconnect">Disconnect</a></div>';
		    } else {
                echo '<script>alert(\"You are not logged in\");window.location.replace(\"?page=/\")</script>';
		    }
    	?>
        <div class="content">  
            <h2><label for="file">Charger votre fichier d'activité sportive</label></h2>
            <div class="upload_form">
                <form method="post" action="?page=upload_activity_form" enctype='multipart/form-data'>
                    <input name="file" accept=".json" type="file" required/><br/>
                    <input type="submit" name="upload" value="Envoyer"/>
                </form>
            </div>
        </div>

        <div class="footer">
            <p>site created by 2 IUT students</p>
        </div>
    </body>
</html>