<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/css/stylesheet.css">
        <link href="/contents/icone.png" type="image/png" rel="icon">
        <title>SportTrack - Activities</title>
    </head>
    <body>

        <div class="head">
            <img src="/contents/logo.png" width="125" height="125" rel="logo" type="image/png">
            <h1>SportTrack</h1>
        </div>
        <?php
		    if(isset($_SESSION["user"])) {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		        echo '</br><a href="?page=upload_activity_form">Upload a file</a>';
                echo '</br><a href="?page=list_activities">List of Activities</a>';
		        echo '</br><a href="?page=modify_user_form">Change your data</a>';
                echo '</br><a href="?page=user_disconnect">Disconnect</a></div>';
		    } else {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		    	echo '</br><a href="?page=user_add_form">Register</a>';
		    	echo '</br><a href="?page=user_connect">Login</a></div>';
		    }
    	?>
        <div class="content">  
            <?php
                if(!isset($_SESSION["user"])) {
                    header("Location: /index.php?page=/");
                    die();
                }

                require_once(__DIR__ . '/../model/DataDAO.php');

                if(isset($_SESSION["activities"])) {
                    echo "<table id=\"tableau\">";
                    echo "<tr class=\"name\">";
                    echo "<th class=\"col\"> Id </th> <th class=\"col\"> Date </th> <th class=\"col\"> Description </th> <th class=\"col\"> Average cardio-frequency</th> <th class=\"col\"> Duration </th> <th class=\"col\"> Distance </th>" ;
                    echo "</tr>";
                    foreach($_SESSION["activities"] as $activity) {
                        $dao = DataDAO::getInstance();
                        $data = $dao->getDataByActivity($activity->getId())[0];
                        echo "<tr>";
                        echo "<th class=\"col\">" . $activity->getId() . "</th class=\"col\">" . "<th class=\"col\">". date('D d M Y', $activity->getDate()) ."</th>" . "<th class=\"col\">". $activity->getDescription() ."</th>";
                        echo "<th class=\"col\">" . $data->getCardioFrequency() . "</th> <th class=\"col\">" . $data->getTime() . "</th> <th class=\"col\">". $data->getDistance() . "</th>";
                        echo "</tr>";
                    }
                    echo "</table>";
                }
            ?>
        </div>

        <div class="footer">
            <p>site created by 2 IUT students</p>
        </div>
    </body>
</html>
