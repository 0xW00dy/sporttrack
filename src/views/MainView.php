<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/css/stylesheet.css">
        <link href="/contents/icone.png" type="image/png" rel="icon">
        <title> 
            SportTrack
        </title>
    </head>

    <body>
        <div class="head">
            <img src="/contents/logo.png" width="125" height="125" rel="logo" type="image/png">
            <h1>SportTrack</h1>
        </div>
        <?php
		    if(isset($_SESSION["user"])) {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		        echo '</br><a href="?page=upload_activity_form">Upload a file</a>';
                echo '</br><a href="?page=list_activities">List of Activities</a>';
		        echo '</br><a href="?page=modify_user_form">Change your data</a>';
                echo '</br><a href="?page=user_disconnect">Disconnect</a></div>';
		    } else {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		    	echo '</br><a href="?page=user_add_form">Register</a>';
		    	echo '</br><a href="?page=user_connect">Login</a></div>';
		    }
    	?>
        <div class="content">  
            <h2>Track your runs in 1 click with our new SportTrack web application</h2>
            </br></br></br></br>
            <h2>What is SportTrack ?</h2>
            <p>It is a web application that allows athletes to track and manage position and heart rate data from their cardio/gps watch</p>
        </div>

        <div class="footer">
            <p>site created by 2 IUT students</p>
        </div>
    </body>
</html>