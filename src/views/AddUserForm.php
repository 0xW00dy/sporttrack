<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="/css/stylesheet.css">
        <link href="/contents/icone.png" type="image/png" rel="icon">
        <title> 
            SportTrack
        </title>
    </head>

    <body>
        <div class="head">
            <img src="/contents/logo.png" width="125" height="125" rel="logo" type="image/png">
            <h1>SportTrack</h1>
        </div>
        <?php
		    if(isset($_SESSION["user"])) {
		        echo "<h2>Bonjour " . $_SESSION["user"][0]->getFirstName() . " !</h2>";
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		        echo '</br><a href="?page=upload_activity_form">Upload a file</a>';
		        echo '</br><a href="?page=modify_user_form">Change your data</a>';
                echo '</br><a href="?page=user_disconnect">Disconnect</a></div>';
		    } else {
                echo '<div class="navbar"></br><a href="?page=/">Home</a>';
		    	echo '</br><a href="?page=user_add_form">Register</a>';
		    	echo '</br><a href="?page=user_connect">Login</a></div>';
		    }
    	?>
        <div class="content">  
            <h1>SportTrack</h1>
            <h2>Register</h2>
            <div class="data_form">
                <form method="post">
                    First Name<input type="text" name="firstname"><br />
                    Last Name<input type="text" name="lastname"><br />
                    Email<input type="email" name="email"><br />
                    Birthday<input type="date" name="birthdate">
                    Gender<select name="gender">
                                <option disabled="disabled" selected="selected">Choose option</option>
                                <option>M</option>
                                <option>F</option>
                                <option>O</option>
                    </select><br />
                    Height<input type="number" name="size"><br />
                    Weight<input type="number" name="weight"><br />
                    Password <input type="password" name="password"> <br />
                    Confirm Password<input type="password" name="confirm"><br />
                    <input type="submit" value="Enregistrer"/>
                </form>
            </div>
        </div>

        <div class="footer">
            <p>site created by 2 IUT students</p>
        </div>
    </body>
</html>