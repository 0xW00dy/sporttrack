-- User (idUser (1), lastName (NN), firstName (NN), birthday (NN), gender (NN), size (NN), weight (NN), email (NN), password (NN))
-- Activity (idActivity (1), date (NN), description (NN), theUser = @User.idUser (NN))
-- Data (idData (1), time (NN), cardio_frequency (NN), latitude(NN), longitude (NN), altitude (NN), theActivity = @Activity.idActivity (NN))


DROP TABLE IF EXISTS data;
DROP TABLE IF EXISTS activity;
DROP TABLE IF EXISTS users;


CREATE TABLE users (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	firstname VARCHAR(50)
		NOT NULL,
	lastname VARCHAR(50)
		NOT NULL,
	birthday DATE
		NOT NULL,
	gender VARCHAR(1)
		NOT NULL
		CHECK (gender = 'F' OR gender = 'M' or gender='O'),
	height INTEGER
		NOT NULL
		CHECK (height between 50 and 250),
	weight INTEGER
		NOT NULL
		CHECK (weight between 5 and 300),
	email VARCHAR(50)
		NOT NULL
	    UNIQUE
		CHECK (email LIKE '%@%.%'),
	password VARCHAR(64)
		NOT NULL
);

CREATE TABLE activity (
	idActivity INTEGER PRIMARY KEY AUTOINCREMENT,
	dateActivity DATE NOT NULL,
	description VARCHAR(300) NOT NULL,
	idUser INTEGER NOT NULL,
	FOREIGN KEY(idUser) REFERENCES users(id)
);

CREATE TABLE data (
	idData INTEGER PRIMARY KEY AUTOINCREMENT,
	duration INTEGER
        NOT NULL, -- en minutes
	cardioFreqMin INTEGER
        CHECK (cardioFreqMin BETWEEN 0 AND 220),
    cardioFreqMax INTEGER(3)
        CHECK (cardioFreqMax BETWEEN 0 AND 220),
    cardioFreqAvg INTEGER(3)
        CHECK (cardioFreqAvg BETWEEN cardioFreqMin AND cardioFreqMax),
    distance INTEGER
        NOT NULL,
    idActivity INTEGER NOT NULL,
	FOREIGN KEY(idActivity) REFERENCES activity(idActivity)
);

-- TRIGGERS

CREATE TRIGGER IF NOT EXISTS trig_insertVerifCardioFreq
BEFORE INSERT ON data
BEGIN
    SELECT CASE
    WHEN NEW.cardioFreqMin > NEW.cardioFreqAvg THEN
        RAISE(ABORT, 'Error: The minimum heart frequency is greater than the avergage frequency !')
    END;

    SELECT CASE
    WHEN NEW.cardioFreqMax < NEW.cardioFreqAvg THEN
        RAISE(ABORT, 'Error: The maximun heart frequency is lower than the avergage frequency !')
    END;

END;


CREATE TRIGGER IF NOT EXISTS trig_updateVerifCardioFreq
AFTER UPDATE ON data
BEGIN

    SELECT CASE
    WHEN NEW.cardioFreqMin > NEW.cardioFreqAvg THEN
        RAISE(ABORT, 'Error: The minimum heart frequency is greater than the avergage frequency !')
    END;

    SELECT CASE
    WHEN NEW.cardioFreqMax < NEW.cardioFreqAvg THEN
        RAISE(ABORT, 'Error: The maximun heart frequency is lower than the avergage frequency !')
    END;

END;
