<?php

    require_once('SqliteConnection.php');
    require_once('User.php');

    class UserDAO {
        private static $dao;

        private function __construct() {}

        public final static function getInstance() {
            if(!isset(self::$dao)) self::$dao = new UserDAO();
            return self::$dao;
        }

        public final function findAll() {
            $dbc = SqliteConnection::getInstance()->getConnection();
            $query = "SELECT * FROM Users;";
            $stmt = $dbc->prepare($query);
            $stmt->execute();
            $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'User');
            return $results;
        }

        public final function findUser($email) {
            $dbc = SqliteConnection::getInstance()->getConnection();
            $query = "SELECT * FROM Users WHERE email = :e;";
            $stmt = $dbc->prepare($query);

            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->execute();

            $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'User');
            return $results;
        }

        public final function getEmail(){

            $dbc = SqliteConnection::getInstance()->getConnection();

            $query = "SELECT email FROM Users";
            $stmt = $dbc->query($query);
            $results = $stmt->fetchALL();
            return $results;
        }

        public final function checkUser($email, $pwd) {
            $dbc = SqliteConnection::getInstance()->getConnection();
            $query = "SELECT * FROM Users WHERE email = :e AND password = :p;";
            $stmt = $dbc->prepare($query);

            $stmt->bindValue(':e', $email, PDO::PARAM_STR);
            $stmt->bindValue(':p', $pwd, PDO::PARAM_STR);
            $stmt->execute();

            $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'User')[0];
            return $results;
        }

        public final function insert($u){
            if($u instanceof User){
                $dbc = SqliteConnection::getInstance()->getConnection();
                // prepare the SQL statement
                $query = "INSERT INTO Users(firstName, lastName, birthday, gender, height, weight, email,
                    password) VALUES (:fn,:ln,:bd,:g,:s,:w,:e,:p);";
                $stmt = $dbc->prepare($query);

                // bind the paramaters
                $stmt->bindValue(':fn',$u->getFirstName(),PDO::PARAM_STR);
                $stmt->bindValue(':ln',$u->getLastName(),PDO::PARAM_STR);
                $stmt->bindValue(':bd',$u->getBday(),PDO::PARAM_STR);
                $stmt->bindValue(':g',$u->getGender(),PDO::PARAM_STR);
                $stmt->bindValue(':s',$u->getHeight(),PDO::PARAM_INT);
                $stmt->bindValue(':w',$u->getWeight(),PDO::PARAM_INT);
                $stmt->bindValue(':e',$u->getEmail(),PDO::PARAM_STR);
                $stmt->bindValue(':p',$u->getPassword(),PDO::PARAM_STR);

                // execute the prepared statement
                $stmt->execute();
            }else throw new Exception("Passed parameter object need to be User object !");
        }

        public final function update($u) {
            if($u instanceof User){
                $dbc = SqliteConnection::getInstance()->getConnection();
                // prepare the SQL statement
                $query = "UPDATE Users SET lastName = :ln, firstName = :fn, birthday = :bd, gender = :g, size = :s, weight = :w, email = :e,
                password = :p WHERE firstName = :fn AND lastName = :ln";
                $stmt = $dbc->prepare($query);

                // bind the paramaters
                $stmt->bindValue(':ln',$u->getLastName(),PDO::PARAM_STR);
                $stmt->bindValue(':fn',$u->getFirstName(),PDO::PARAM_STR);
                $stmt->bindValue(':bd',$u->getBday(),PDO::PARAM_STR);
                $stmt->bindValue(':g',$u->getGender(),PDO::PARAM_STR);
                $stmt->bindValue(':s',$u->getHeight(),PDO::PARAM_INT);
                $stmt->bindValue(':w',$u->getWeight(),PDO::PARAM_INT);
                $stmt->bindValue(':e',$u->getEmail(),PDO::PARAM_STR);
                $stmt->bindValue(':p',$u->getPassword(),PDO::PARAM_STR);

                // execute the prepared statement
                $stmt->execute();
            } else throw new Exception("Passed parameter object need to be User object !");
        }

        public final function delete($u) {
            if($u instanceof User){
                $dbc = SqliteConnection::getInstance()->getConnection();
                // prepare the SQL statement
                $query = "DELETE FROM Users WHERE firstName = :fn AND lastName = ln";
                $stmt = $dbc->prepare($query);

                // bind the paramaters
                $stmt->bindValue(':fn',$u->getFirstName(),PDO::PARAM_STR);
                $stmt->bindValue(':ln',$u->getLastName(),PDO::PARAM_STR);

                // execute the prepared statement
                $stmt->execute();
            }else throw new Exception("Passed parameter object need to be User object !");
        }

        public final static function deleteAll() {
            $dbc = SqliteConnection::getInstance()->getConnection();
            $query = "DELETE FROM Users;";
            $stmt = $dbc->prepare($query);
            $stmt->execute();
        }

    }
?>
