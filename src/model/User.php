<?php

class User {
  private $id;
  private $firstname;
  private $lastname;
  private $birthday;
  private $gender;
  private $height;
  private $weight;
  private $email;
  private $password;

  public function __construct() {}

  public function init(string $fn, string $ln, string $bd, string $g, int $h, int $w, string $e, string $p) {
    $this->firstname = $fn;
    $this->lastname = $ln;
    $this->birthday = $bd;
    $this->gender = $g;
    $this->height = $h;
    $this->weight = $w;
    $this->email = $e;
    $this->password = $p;
  }

  public function getIdUser(){
    return $this->id;
  }
  public function getFirstName(){
    return $this->firstname;
  }
  public function getLastName(){
    return $this->lastname;
  }
  public function getBday(){
    return $this->birthday;
  }
  public function getGender(){
    return $this->gender;
  }
  public function getHeight(){
    return $this->height;
  }
  public function getWeight(){
    return $this->weight;
  }
  public function getEmail(){
    return $this->email;
  }
  public function getPassword(){
    return $this->password;
  }

  public function setIdUser(int $i) : void { $this->id = $i; }
  public function setFirstName(string $fname) : void { $this->firstname = $fname; }
  public function setLastName(string $lname) : void { $this->lastname = $lname; }
  public function setBirthday(string $birthday) : void { $this->birthday = $birthday; }
  public function setGender(string $gender) : void { $this->gender = $gender; }
  public function setHeight(int $height) : void { $this->height = $height; }
  public function setWeight(int $weight) : void { $this->weight = $weight; }
  public function setEmail(string $email) : void { $this->email = $email; }
  public function setPassword(string $password) : void { $this->password = $password; }

  /**
 * toString method
 * @return string a string representation of the User object
 */
 public function __toString() : string {
    return $this->email . " (" . $this->firstname . " " . $this->lastname . ")";
  }

}

?>
