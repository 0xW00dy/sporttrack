<?php
    require_once(__DIR__ . "/CalculDistanceImpl.php");
    require_once(__DIR__ . "/Activity.php");
    require_once(__DIR__ . "/Data.php");
    require_once(__DIR__ . "/ActivityDAO.php");
    require_once(__DIR__ . "/DataDAO.php");
    
    class JSONUtility {
        private $calc_dist;

        public function __construct(){}

        public function init($json) {
            $this->calc_dist = new CalculDistanceImpl();
            $this->jsonActivity($json);
            $this->jsonData($json);
        }

        private function jsonActivity($parsed_json) {
            $activity = new Activity();

            $date = strtotime($parsed_json->{"activity"}->{"date"});
            $desc = $parsed_json->{"activity"}->{"description"};

            $maxIdA = ActivityDAO::getInstance()->getMaxId()[0]["maxID"];
            $activity->init($maxIdA+1,$date, $desc, $_SESSION["user"][0]->getIdUser()); // TODO: vérif si bon id utilisé

            $dao = ActivityDAO::getInstance();
            $dao->insert($activity);
        }

        private function jsonData($parsed_json) {
            $data = new Data();

            $dist = $this->calculateDist($parsed_json);
            $duration = $this->calculateDuration($parsed_json);
            $cardio = $this->calculateCardio($parsed_json);
            $maxIdA = ActivityDAO::getInstance()->getMaxId()[0]["maxID"];
            $maxIdD = DataDAO::getInstance()->getMaxId()[0]["maxID"];
            $data->init($maxIdD+1, $duration, $cardio, $dist, $maxIdA); // TODO: vérif si bon id utilisé

            $dao = DataDAO::getInstance();
            $dao->insert($data);
        }

        private function calculateCardio($parsed_json) {
            $data = $parsed_json->{"data"};

            $cardio = 0;
            foreach ($data as $value)
            {
                $cardio += $value->{"cardio_frequency"};
            }

            return ($cardio/count($data));
        }

        private function calculateDist($parsed_json) {
            $data = $parsed_json->{"data"};

            $parcours = array();
            foreach ($data as $key => $value)
            {
                $lat = $value->{"latitude"};
                $long = $value->{"longitude"};

                array_push($parcours, array($lat, $long));
            }
            return $this->calc_dist->calculDistanceTrajet($parcours);
        }

        private function calculateDuration($parsed_json) {
            $data = $parsed_json->{"data"};

            $duration = 0;
            if(count($data) > 1) {
                $last = strtotime($data[count($data) - 1]->{"time"});
                $first = strtotime($data[0]->{"time"});
                $duration = $last - $first;
            }
            return $duration;
        }
    }
?>