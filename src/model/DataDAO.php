<?php

require_once("Data.php");

class DataDAO
{
    private static $dao;

    private function __construct()
    {
    }

    public final static function getInstance()
    {
        if(!isset(self::$dao)) self::$dao = new DataDAO();
        return self::$dao;
    }

    public final static function getDataByActivity($idActivity) {
        $dbc = Sqliteconnection::getInstance()->getConnection();

        $query = "SELECT * FROM data WHERE idActivity = :ia;";
        $stmt = $dbc->prepare($query);

        $stmt->bindValue(":ia", $idActivity, PDO::PARAM_INT);
        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_CLASS, 'Data');
        return $results;
    }

    public final function findAll()
    {
        $dbc = SqliteConnection::getInstance()->getConnection();

        $query = "select * from data order by idData";
        $stmt = $dbc->query($query);
        $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Data');
        return $results;
    }

    public final function getMaxId() {
        $dbc = SqliteConnection::getInstance()->getConnection();
        $query = "SELECT COUNT(*) AS maxID FROM Data";
        $stmt = $dbc->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
      }

    public final function insert($data)
    {
        if($data instanceof Data) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            $query = "INSERT INTO data(duration, cardioFreqAvg, distance, idActivity) values (:d, :cf, :dist,:idA);";
            $stmt = $dbc->prepare($query);

            $stmt->bindValue(':d', $data->getTime(), PDO::PARAM_INT);
            $stmt->bindValue(':cf', $data->getCardioFrequency(), PDO::PARAM_INT);
            $stmt->bindValue(':dist', $data->getDistance(), PDO::PARAM_INT);
            $stmt->bindValue(':idA', $data->getFkActivity(), PDO::PARAM_INT);

            $stmt->execute();
        }
    }

    public final function update($data)
    {
        if($data instanceof Data) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            $query = "UPDATE data
        SET duration = :d,
        cardioFreqAvg = :cf,
        distance = :dist;";
            $stmt = $dbc->prepare($query);

            $stmt->bindValue(':d', $data->getTime(), PDO::PARAM_INT);
            $stmt->bindValue(':cf', $data->getCardioFrequency(), PDO::PARAM_INT);
            $stmt->bindValue(':dist', $data->getDistance(), PDO::PARAM_INT);

            $stmt->execute();
        }
    }

    public final function delete($data)
    {
        if($data instanceof Data) {
            $dbc = SqliteConnection::getInstance()->getConnection();

            $query = "DELETE FROM Data WHERE idData = :id;";
            $stmt = $dbc->prepare($query);

            $stmt->bindValue(':id', $data->getId(), PDO::PARAM_INT);

            $stmt->execute();
        }
    }
}

?>
