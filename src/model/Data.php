<?php

class Data
{
    private $idData;
    private $duration;
    private $cardioFreqAvg;
    private $distance;
    private $fk_activity;

    public function __construct()
    {
    }

    public function init($id, $t, $cardio, $dist, $fk)
    {
        $this->idData           = $id;
        $this->time             = $t;
        $this->cardioFreqAvg    = $cardio;
        $this->distance             = $dist;
        $this->fk_activity      = $fk;
    }

    public function setId($id)
    {
        $this->idData = $id;
    }

    public function setTime($t)
    {
        $this->duration = $t;
    }

    public function setCardioFrequency($cardio)
    {
        $this->cardioFreqAvg = $cardio;
    }

    public function setFkActivity($fk)
    {
        $this->fk_activity = $fk;
    }

    public function getId()
    {
        return $this->idData;
    }

    public function getTime()
    {
        return $this->duration;
    }

    public function getCardioFrequency()
    {
        return $this->cardioFreqAvg;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function getFkActivity()
    {
        return $this->fk_activity;
    }

    public function __toString()
    {
        return "idData : " . $this->idData .
            "\nTime : " . $this->time . "\nCardio Frequency : " . $this->cardio_frequency .
            "\nLatitude : " . $this->latitude . "\nLongitude : " . $this->longitude .
            "\nAltitude : " . $this->altitude;
    }
}

?>
