<?php

require_once(__DIR__  . "/CalculDistance.php");

class CalculDistanceImpl implements CalculDistance {

    public function __construct() {}

    /**
     * Return the distance in meters between 2 GPS coordinates.
     * @param float $lat1 Latitude of the first coord
     * @param float $long1 Longitude of the first coord
     * @param float $lat2 Latitude of the second coord
     * @param float $long2 Longitude of the second coord
     * @return float The distance between the 2 coordinates
     */
    public function calculDistance2PointsGPS($lat1, $long1, $lat2, $long2) : float {
  $earth_radius = 6378137;

	$rad_lat1 = deg2rad($lat1);	
	$rad_lng1 = deg2rad($long1);
	$rad_lat2 = deg2rad($lat2);
	$rad_lng2 = deg2rad($long2);

	$dla = ($rad_lat2 - $rad_lat1)/2;
	$dlo = ($rad_lng2 - $rad_lng1)/2;
	$a = (sin($dla) * sin($dla)) + cos($rad_lat1) * cos($rad_lat2) * (sin($dlo) * sin($dlo));
	$d = 2 * atan2(sqrt($a), sqrt(1 - $a));

	return $earth_radius * $d;

    }

    /**
     * Returns the total distance of an activity
     * @param Array<Array> An array of activity entries
     * @return float The total distance of the activity
     */
    public function calculDistanceTrajet(Array $parcours) : float {
        $distTotal = 0;

        for ($i = 0; $i < sizeof($parcours)-1; $i++) {
            $pos1 = $parcours[$i];
            $pos2 = $parcours[$i+1];

            $distTotal += $this->calculDistance2PointsGPS($pos1[0], $pos1[1], $pos2[0], $pos2[1]);
        }

        return $distTotal;
    }

}

?>
