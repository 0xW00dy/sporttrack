<?php

    class Activity{
        private $idActivity;
        private $dateActivity;
        private $description;
        private $idUser;

        public function  __construct() { }

        public function init($id, $d, $des, $idUser){
            $this->idActivity = $id;
            $this->dateActivity= $d;
            $this->description = $des;
            $this->idUser = $idUser;
        }

        public function setId($id){ $this->idActivity = $id; }
        public function setDate($d){ $this->dateActivity = $d; }
        public function setDescription($des){ $this->description = $des; }
        public function setIdUser($id){ $this->idUser = $id; }

        public function getId(){ return $this->idActivity; }
        public function getDate(){ return $this->dateActivity; }
        public function getDescription(){ return $this->description; }
        public function getIdUser(){return $this->idUser; }
        public function  __toString() { return "idActivity : ". $this->idActivity.
            "\nDate : ". $this->date. "\nDescription : ". $this->description; }
    }
?>
