<?php

define("SQLITE_DATABASE_PATH", __DIR__."/../SportTrack.db");
ini_set('display_errors', 'On');
error_reporting(E_ALL);

class SqliteConnection {
  private static $instance;

  private function __construct() {}

  public static final function getInstance() {
    if(!isset(self::$instance)) {
      self::$instance = new SqliteConnection();
    }
    return self::$instance;
  }

  /**
  * Get Connection to database
  * @return PDO the database connection object
  */
  public static function getConnection(): PDO {
    $connection = null;
    try {
      $connection = new PDO("sqlite:". SQLITE_DATABASE_PATH);
      $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $err) {
      exit("Error: " . $err->getMessage());
    }

    return $connection;
  }
}
?>