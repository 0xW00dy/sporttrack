<?php

require_once('SqliteConnection.php');
require_once('Activity.php');

class ActivityDAO {
  private static $dao;

  private function __construct(){}

  public final static function getInstance() {
    if(!isset(self::$dao)) {
      self::$dao = new ActivityDAO();
    }
    return self::$dao;
  }

  public final function getActivitiesOfUser($id) {
      $dbc = SqliteConnection::getInstance()->getConnection();
      $query = "SELECT * FROM activity where idUser = :id;";
      $stmt = $dbc->prepare($query);

      $stmt->bindValue(':id', $id, PDO::PARAM_INT);
      $stmt->execute();

      $results = $stmt->fetchAll(PDO::FETCH_CLASS, 'Activity');

      return $results;
  }

  public final function findAll() {
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from activity";
    $stmt = $dbc->query($query);
    $results = $stmt->fetchAll(PDO::FETCH_CLASS, 'Activity');
    return $results;
  }

  public final function getId($date, $description) {
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "SELECT * FROM activity WHERE date = :da AND description = :des";
    $stmt = $dbc->prepare($query);
    $stmt->bindValue(':da', $date, PDO::PARAM_STR);
    $stmt->bindValue(':des', $description, PDO::PARAM_STR);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_CLASS, 'Activity');
    return $results;
  }

  public final function getMaxId() {
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "SELECT COUNT(*) AS maxID FROM Activity";
    $stmt = $dbc->query($query);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  public final function insert($act) {
    if($act instanceof Activity) {
      $dbc = SqliteConnection::getInstance()->getConnection();

      $query = "INSERT INTO Activity(idActivity, dateActivity, description, idUser) values (:iA, :dA, :d, :iU);";
      $stmt = $dbc->prepare($query);

      $stmt->bindValue(':iA', $act->getId(), PDO::PARAM_INT);
      $stmt->bindValue(':dA', $act->getDate(), PDO::PARAM_STR);
      $stmt->bindValue(':d', $act->getDescription(), PDO::PARAM_STR);
      $stmt->bindValue(':iU', $act->getIdUser(), PDO::PARAM_INT);

      $stmt->execute();
    }
  }

  public final function update($act) {
    if($act instanceof Activity) {
      $dbc = SqliteConnection::getInstance()->getConnection();

      $query = "UPDATE activity
      SET dateActivity = :a,
      description = :d,
      idUser = :i
      WHERE idActivity = :id;";

      $stmt = $dbc->prepare($query);

      $stmt->bindValue(':a', $act->getDate(), PDO::PARAM_STR);
      $stmt->bindValue(':d', $act->getDescription(), PDO::PARAM_STR);
      $stmt->bindValue(':i', $act->getIdUser(), PDO::PARAM_INT);
      $stmt->bindValue(':id', $act->getId(), PDO::PARAM_INT);

      $stmt->execute();
    }
  }

  public final function delete($act) {
    if($act instanceof Activity) {
      $dbc = SqliteConnection::getInstance()->getConnection();

      $query = "DELETE FROM activity WHERE idActivity = :id;";
      $stmt = $dbc->prepare($dbc);

      $stmt->bindValue(':id', $act->getId(), PDO::PARAM_INT);

      $stmt->execute();
    }
  }


}
 ?>
