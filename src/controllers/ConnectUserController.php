<?php
    error_reporting(E_ALL & ~E_NOTICE);
    require_once("Controller.php");
    require_once(__DIR__ . "/../model/User.php");
    require_once(__DIR__ . "/../model/UserDAO.php");

    /*htmlentities*/

    class ConnectUserController implements Controller {
        public function handle($request) {
            if($_SERVER["REQUEST_METHOD"] === "POST" and !isset($_SESSION["user"])) {
                if(isset($_SESSION["user"])) {
                    unset($_SESSION["user"]);
                }
                $email = $request['email'];
                $hashed = hash("sha256", $request["pwd"]);

                $dao = UserDAO::getInstance();

                if(!$dao->checkUser($email, $hashed)) {
                    echo "<script>alert(\"Incorrect login or password !\");window.location.replace(\"?page=user_connect\")</script>";
                }else {
                    $_SESSION["user"] = $dao->findUser($email);
                    echo "<script>alert(\"You're logged !\");window.location.replace(\"?page=/\")</script>";
                }
            }
        }
    }
    //test@test.fr:testtest12
?>
