<?php
require_once('Controller.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/UserDAO.php');

class ModifyUserController implements Controller {
    /**
     * Verify if the email's format is valid
     * @param string $email The email to verify
     * @return bool        If the email is valid
     */
    public function emailIsValid(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Verify if the password's format is valid
     * @param string $password The password to verify
     * @return bool        If the email is valid
     */
    public function passwordIsValid(string $password): bool
    {
        return !empty($password) && strlen($password) >= 8;
    }

    /**
     * Verify if the name's format is valid
     * @param string $firstName The firstname to verify
     * @return bool        If the firstName is valid
     */
    public function nameIsValid(string $name): bool
    {
        return !empty($name) && preg_match("/^[a-z]+$/i", $name);
    }


    /**
     * Verify if the birthday's format is valid
     * @param string $birthday The birthday to verify
     * @return bool        If the birthday is valid
     */
    public function birthdayIsValid(string $birthday): bool
    {
        // check YYYY-MM-DD
        return !empty($birthday) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $birthday);
    }

    /**
     * Verify if the gender's format is valid
     * @param string $gender The gender to verify
     * @return bool        If the gender is valid
     */
    public function genderIsValid(string $gender): bool
    {
        return !empty($gender) && in_array(strtoupper($gender), array("M", "F", "O"));
    }

    /**
     * Verify if the height's format is valid
     * @param string $height The height to verify
     * @return bool        If the height is valid
     */
    public function heightIsValid(string $height): bool
    {
        return is_numeric($height) && (int)$height >= 10 && (int)$height <= 300;
    }

    /**
     * Verify if the weight's format is valid
     * @param string $weight The weight to verify
     * @return bool        If the weight is valid
     */
    public function weightIsValid(string $weight): bool
    {
        return is_numeric($weight) && (int)$weight >= 5 && (int)$weight <= 300;
    }


    public function handle($request) {
        if(!isset($_SESSION["user"])){
            header("Location: /index.php?page=user_connect");
        }
        if($_SERVER["REQUEST_METHOD"] === "POST" and isset($_SESSION["user"])) {
            $user = $_SESSION["user"][0];
            $user_password = $user->getPassword();
            $errors = null;

            $userDAO = UserDAO::getInstance();

            if(isset($request["lastname"])){
                $name = $request["lastname"];
                if($this->nameIsValid($name)) {
                    $user->setLastName($name);
                } else {
                    $errors .= "Invalid last name, please only use characters." . PHP_EOL;
                }
            }
            if(isset($request["firstname"])){
                $firstname = $request["firstname"];
                if($this->nameIsValid($firstname)) {
                    $user->setLastName($firstname);
                } else {
                    $errors .= "Invalid first name, please only use characters." . PHP_EOL;
                }
            }
            if(isset($request["birthday"])) {
                $bday = $request["birthday"];
                if($this->birthdayIsValid($bday)) {
                    $user->setBirthday($bday);
                } else {
                    $errors .= "Invalid Birthday, please use this format : YYYY-MM-DD" . PHP_EOL;
                }
            }
            if(isset($request["gender"])) {
                $gender = $request["gender"];
                if($this->genderIsValid($gender)) {
                    $user->setGender($gender);
                } else {
                    $errors .= "Invalid gender, please choose between MAN, WOMAN and OTHER !" . PHP_EOL;
                }
            }
            if(isset($request["height"])) {
                $height = $request["height"];
                if($this->heightIsValid($height)) {
                    $user->setHeight($height);
                } else {
                    $errors .= "Invalid height, please choose between 10 and 300cm !" . PHP_EOL;
                }
            }
            if(isset($request["weight"])) {
                $weight = $request["weight"];
                if($this->weightIsValid($weight)) {
                    $user->setWeight($weight);
                } else {
                    $errors .= "Invalid Weight, please choose between 5 and 300kg !" . PHP_EOL;
                }
            }
            if(isset($request["email"])) {
                $email = $request["email"];
                if($this->emailIsValid($email)) {
                    if(!$userDAO->findUser($email)) {
                        $user->setEmail($email);
                    } else {
                        $errors .= "Error. Email address already in use." . PHP_EOL;
                    }
                } else {
                    $errors .= "Invalid email address ! (example@domain.ext)" . PHP_EOL;
                }
            }
            if(isset($request["newpassword"])) {
                $newpassword = $request["newpassword"];
                if($this->passwordIsValid($newpassword)) {
                    $hashed = hash("sha256", $request["password"]);
                    $user->setPassword($hashed);
                } else {
                    $errors .= "Invalid password ! (8 chars min.)" . PHP_EOL;
                }
            }
            if(isset($request["password"])) {
                $hashed = hash("sha256", $request["password"]);
                if($hashed === $user_password) {
                    $_SESSION["user"] = $user;
                    $userDAO->insert($user);
                }
            } else {
                $errors .= "Invalid password. Operation canceled.";
            }
        }
    }
}