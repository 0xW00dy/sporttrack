<?php
require_once('Controller.php');
require_once(__DIR__ . '/../model/User.php');
require_once(__DIR__ . '/../model/UserDAO.php');

class AddUserController implements Controller
{

    /**
     * Verify if the email's format is valid
     * @param string $email The email to verify
     * @return bool        If the email is valid
     */
    public function emailIsValid(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Verify if the password's format is valid
     * @param string $password The password to verify
     * @return bool        If the email is valid
     */
    public function passwordIsValid(string $password): bool
    {
        return !empty($password) && strlen($password) >= 8;
    }

    /**
     * Verify if the firstname's format is valid
     * @param string $firstName The firstname to verify
     * @return bool        If the firstName is valid
     */
    public function firstNameIsValid(string $firstName): bool
    {
        return !empty($firstName) && preg_match("/^[a-z]+$/i", $firstName);
    }

    /**
     * Verify if the lastname's format is valid
     * @param string $lastName The lastname to verify
     * @return bool        If the lastname is valid
     */
    public function lastNameIsValid(string $lastName): bool
    {
        return !empty($lastName) && preg_match("/^[a-z]+$/i", $lastName);
    }

    /**
     * Verify if the birthday's format is valid
     * @param string $birthday The birthday to verify
     * @return bool        If the birthday is valid
     */
    public function birthdayIsValid(string $birthday): bool
    {
        // check YYYY-MM-DD
        return !empty($birthday) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $birthday);
    }

    /**
     * Verify if the gender's format is valid
     * @param string $gender The gender to verify
     * @return bool        If the gender is valid
     */
    public function genderIsValid(string $gender): bool
    {
        return !empty($gender) && in_array(strtoupper($gender), array("M", "F", "O"));
    }

    /**
     * Verify if the height's format is valid
     * @param string $height The height to verify
     * @return bool        If the height is valid
     */
    public function heightIsValid(string $height): bool
    {
        return is_numeric($height) && (int)$height >= 10 && (int)$height <= 300;
    }

    /**
     * Verify if the weight's format is valid
     * @param string $weight The weight to verify
     * @return bool        If the weight is valid
     */
    public function weightIsValid(string $weight): bool
    {
        return is_numeric($weight) && (int)$weight >= 5 && (int)$weight <= 300;
    }

    /**
     * This function handles a request to create a user.
     */
    public function handle($request)
    {
        if(isset($_SESSION)) unset($_SESSION["error"]);
        else {
            session_start();
        }

        if(isset($_SESSION["user"])) {
            unset($_SESSION["user"]);
        }

        if($_SERVER["REQUEST_METHOD"] === "POST"){
            $errors = null;

            if(!$this->emailIsValid($request["email"])) $errors .= "Invalid email address ! (example@domain.ext)" . PHP_EOL;
            if(!$this->passwordIsValid($request["password"])) $errors .= "Invalid password ! (8 chars min.)" . PHP_EOL;
            if($request["password"] != $request["confirm"]) $errors .= "Password confirmation is different from password." . PHP_EOL;
            if(!$this->firstNameIsValid($request["firstname"])) $errors .= "Invalid First Name, please only use characters." . PHP_EOL;
            if(!$this->lastNameIsValid($request["lastname"])) $errors .= "Invalid Last Name, please only use characters." . PHP_EOL;
            if(!$this->birthdayIsValid($request["birthdate"])) $errors .= "Invalid Birthday, please use this format : YYYY-MM-DD" . PHP_EOL;
            if(!$this->genderIsValid($request["gender"])) $errors .= "Invalid gender, please choose between MAN, WOMAN and OTHER !" . PHP_EOL;
            if(!$this->heightIsValid($request["size"])) $errors .= "Invalid Height, please choose between 10 and 300cm !" . PHP_EOL;
            if(!$this->weightIsValid($request["weight"])) $errors .= "Invalid Weight, please choose between 5 and 300kg !" . PHP_EOL;

            $userDAO = UserDAO::getInstance();

            if($userDAO->findUser($request["email"])) $errors .= "This email address is already in use." . PHP_EOL;

            if($errors === null){
                $user   = new User();
                $hashed = hash("sha256", $request["password"]);
                $user->init($request["firstname"], $request["lastname"], $request["birthdate"], $request["gender"], $request["size"], $request["weight"], $request["email"], $hashed);

                $userDAO->insert($user);
                if(isset($_SESSION["error"])) unset($_SESSION["error"]);
                echo "<script>alert('Register successful ! You can now login.'); document.location='/index.php?page=user_connect';</script>";
            } else {
                $_SESSION["error"] = substr($errors, 0);
                echo $_SESSION["error"];
            }
        }
    }

}

?>
