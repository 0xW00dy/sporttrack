<?php
    require_once("Controller.php");
    require_once(__DIR__ . "/../model/Activity.php");
    require_once(__DIR__ . "/../model/ActivityDAO.php");
    require_once(__DIR__ . "/../model/JSONUtility.php");

    class UploadActivityController implements Controller {
        public function handle($request) {
            if($_SERVER["REQUEST_METHOD"] === "POST") {
                if(isset($_FILES["file"])){
                    $error = false;
                    try {
   
                        // Undefined | Multiple Files | $_FILES Corruption Attack
                        // If this request falls under any of them, treat it invalid.
                        if (
                            !isset($_FILES['file']['error']) ||
                            is_array($_FILES['file']['error'])
                        ) {
                            throw new RuntimeException('Invalid parameters.');
                        }
                    
                        // Check $_FILES['file']['error'] value.
                        switch ($_FILES['file']['error']) {
                            case UPLOAD_ERR_OK:
                                break;
                            case UPLOAD_ERR_NO_FILE:
                                throw new RuntimeException('No file sent.');
                            case UPLOAD_ERR_FORM_SIZE:
                                throw new RuntimeException('Exceeded filesize limit.');
                            default:
                                throw new RuntimeException('Unknown errors.');
                        }
                    
                        // You should also check filesize here.
                        if ($_FILES['file']['size'] > 10000) {
                            throw new RuntimeException('Exceeded filesize limit.');
                        }
                    
                        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        //Début des vérifications de sécurité...
                        //Si l'extension n'est pas dans le tableau
                        if($ext != "json"){
                            throw new RuntimeException('Invalid file format.');
                        }
                    
                    
                    } catch (RuntimeException $e) {
                        $error = true;
                        echo $e->getMessage() . "</br>";
                    
                    }

                    if($error == false) {
                        $file = $_FILES['file']['tmp_name'];
                        $json = file_get_contents($file);
                        $parsed_json = json_decode($json);

                        $activity_array = $parsed_json->activity;
                        $data_array = $parsed_json->data;

                        $date = $activity_array->date;
                        $description = $activity_array->description;

                        $dao = ActivityDAO::getInstance();
                        $results = $dao->findAll();
        
                        // variable qui indiquera si les données peuvent être insérées ou non
                        $insert = true;
        
                        // boucle qui vérifie si il existe déjà une activité correspondant aux données que l'utilisateur essaye d'insérer
                        // si la date et l'heure de début correspondent on refuse l'insertion des données
                        /*foreach($results as list ($date_query, $description_query)){
                            if($date == $date_query && $description == $description_query){
                                $insert = false;
                            }
                        }*/

                        if($insert) {
                            /*$maxIdA = ActivityDAO::getInstance()->getMaxId()[0]->getId();
                            echo $maxIdA;
                            echo "</br>";
                            $idU = $_SESSION["user"][0]->getIdUser();
                            echo $idU;
                            $activity = Activity::init($maxIdA, $activity_array[0], $activity_array[1], $idU);
                            echo $activity;
                            $data_dao = DataDAO::getInstance()->fetchAll($data_array);
                            echo $data_dao;
                            $dao->insert($activity);
                            $idA = $dao->getId($date, $description);
                            $dao = DataDAO::getInstance();
                            $dao->insert($data_dao, $idA);*/
                            $jsonUtility = new JSONUtility();
                            $jsonUtility->init($parsed_json);
                        }else {
                            echo "Les données ont déjà été insérées !";
                        }

                    }else {
                        echo "Non Gab tu peux pas :)";
                    }
                }
            }
        }
    }
?>