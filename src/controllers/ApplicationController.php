<?php
class ApplicationController{
    private static $instance;
    private $routes;

    private function __construct(){
        // Sets the controllers and the views of the application.
        $this->routes = [
            '/' => ['controller'=>'MainController', 'view'=>'MainView'], // TODO: write a fancy page
            'user_connect' => ['controller' => 'ConnectUserController', 'view' => 'ConnectUserForm'],  
            'upload_activity_form' => ['controller' => "UploadActivityController", 'view' => 'UploadActivityForm'], 
            'user_add_form' => ['controller' => 'AddUserController', 'view' => 'AddUserForm'], 
            'error' => ['controller'=>null, 'view'=>'ErrorView'], // TODO: write a fancy page
            'user_disconnect' => ['controller'=>'DisconnectUserController', 'view'=>'DisconnectUserVue'],
            'list_activities' => ['controller'=>'ListActivityController', 'view'=>'ListActivityView'], // TODO
            'modify_user_form' => ['controller' =>'ModifyUserController', 'view'=>'ModifyUserView'] // TODO
        ];
    }

    /**
     * Returns the single instance of this class.
     * @return ApplicationController the single instance of this class.
     */
    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new ApplicationController;
        }
        return self::$instance;
    }

    /**
     * Returns the controller that is responsible for processing the request
     * specified as parameter. The controller can be null if their is no data to
     * process.
     * @param Array $request The HTTP request.
     * @param Controller The controller that is responsible for processing the
     * request specified as parameter.
     */
    public function getController($request){
        if(array_key_exists($request['page'], $this->routes)){
            return $this->routes[$request['page']]['controller'];
        }
        return null;
    }

    /**
     * Returns the view that must be return in response of the HTTP request
     * specified as parameter.
     * @param Array $request The HTTP request.
     * @param Object The view that must be return in response of the HTTP request
     * specified as parameter.
     */
    public function getView($request){
        if( array_key_exists($request['page'], $this->routes)){
            return $this->routes[$request['page']]['view'];
        }
        return $this->routes['error']['view'];
    }
}
?>
