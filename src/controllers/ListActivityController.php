<?php
require_once('Controller.php');
require_once(__DIR__ . "/../model/User.php");
require_once(__DIR__ . "/../model/ActivityDAO.php");


class ListActivityController implements Controller {

    public function handle($request) {
        if(!isset($_SESSION["user"])) {
            header("Location: /index.php?page=/");
        } else if ($_SESSION["user"][0] instanceof User){
            $id = $_SESSION["user"][0]->getIdUser();

            $dao = ActivityDAO::getInstance();

            $activities = $dao->getActivitiesOfUser($id);



            $_SESSION["activities"] = $activities;
        }
    }
}
?>