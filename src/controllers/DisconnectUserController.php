<?php
require_once('Controller.php');

class DisconnectUserController implements Controller
{
    /**
     * Handles the $_REQUEST passed as a parameter and disconnects the user
     * if his session is authenticated
     * @param  Array $request The HTTP request to create the user
     */
    public function handle($request): void
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            unset($_SESSION["user"]);
            unset($_SESSION["error"]);
            session_destroy();
            header("Location: /index.php?page=/");
        }
    }
}

?>
